/*
 * Copyright (c) 2011-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include <QObject>
#include <bb/cascades/Application>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/ThemeSupport>
#include <bb/cascades/Theme>
#include <bb/cascades/ColorTheme>
#include <bb/platform/PlatformInfo>

#include "custom/invoker.h"
#include "custom/themesettings.h"
#include "custom/sizehelper.h"
#include "storage/settings.h"
#include "items/stopwatchitem.h"
#include "custom/export.h"
/** Platform features **/
#include "platform/InviteToDownload.hpp"
#include "platform/RegistrationHandler.hpp"
#include "platform/StatusEventHandler.h"
#include "activeframe.h"
#ifdef QT_DEBUG
#include "livecoding/qmlbeam.h"
#endif

namespace bb
{
    namespace cascades
    {
        class LocaleHandler;
    }
}

class QTranslator;

/*!
 * @brief Application UI object
 *
 * Use this object to create and init app UI, to create context objects, to register the new meta types etc.
 */
class ApplicationUI : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool whiteTheme READ whiteTheme CONSTANT)
    Q_PROPERTY(bb::cascades::GroupDataModel *sourceModel READ sourceModel CONSTANT)
    Q_PROPERTY(int sourceModelCount READ sourceModelCount NOTIFY countChanged)
    Q_PROPERTY(StopWatchItem* currentItem READ currentItem WRITE setCurrentItem NOTIFY currentItemChanged)
    Q_PROPERTY(bool newOs READ newOs CONSTANT)
    Q_PROPERTY(bool freeVersion READ freeVersion CONSTANT)
public:
    ApplicationUI();
    virtual ~ApplicationUI();


    Q_INVOKABLE
    inline void deleteLater(QObject *object){
        object->deleteLater();
    }

    Q_INVOKABLE
    void append();
    Q_INVOKABLE
    void removeAll();
    Q_INVOKABLE
    void removeItem(const QVariantList &indexPath);

    Q_INVOKABLE
    void startItem(const QVariantList &indexPath);
    Q_INVOKABLE
    void stopItem(const QVariantList &indexPath);
    Q_INVOKABLE
    void lapItem(const QVariantList &indexPath);
    Q_INVOKABLE
    void resetItem(const QVariantList &indexPath);
    Q_INVOKABLE
    void removeLap(const QVariantList &indexPath, const QVariant &lap);


signals:
    void countChanged();
    void currentItemChanged();

private slots:
    void onSystemLanguageChanged();
    inline bool whiteTheme() const { return Application::instance()->themeSupport()->theme()->colorTheme()->style() == VisualStyle::Bright; }
    inline bool newOs() const { return platformInfo->osVersion().split(".").at(1).toInt() == 3; }
    inline int sourceModelCount() const { return _sourceModel->size(); }
    inline bb::cascades::GroupDataModel *sourceModel() const { return _sourceModel; }

    inline StopWatchItem* currentItem() const { return _currentItem; }
    inline void setCurrentItem(StopWatchItem* value){
        _currentItem = value;
        emit currentItemChanged();
    }
    void thumbnail();
    inline bool freeVersion() const { return _freeVersion; }

    private:
    QTranslator* m_pTranslator;
    bb::cascades::LocaleHandler* m_pLocaleHandler;
    bb::cascades::GroupDataModel *_sourceModel;
    bb::platform::PlatformInfo *platformInfo;
    RegistrationHandler *_registrationHandler;
    InviteToDownload *_inviteDownload;
    Settings *s;
    StopWatchItem* _currentItem;
    SizeHelper *_sizeHelper;
    // ----------
    QmlDocument *qmlCover;
    Container *coverContainer;
    SceneCover *sceneCover;
    StopWatchItem *baseItem;
    bool _freeVersion;
};

#endif /* ApplicationUI_HPP_ */
