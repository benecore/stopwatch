/*
 * sizehelper.h
 *
 *  Created on: 22.12.2013
 *      Author: Benecore
 */

#ifndef SIZEHELPER_H_
#define SIZEHELPER_H_

#include <QObject>
#include <bb/device/DisplayInfo>
#include <bb/device/DeviceInfo>
#include <bb/cascades/OrientationSupport>

using namespace bb::device;
using namespace bb::cascades;

class SizeHelper : public QObject {
    Q_OBJECT
    Q_ENUMS(Orientation)
    Q_PROPERTY(int orientation READ orientation NOTIFY orientationChanged)
    Q_PROPERTY(QString orientationString READ orientationString NOTIFY orientationStringChanged)
    /** Display sizes **/
    Q_PROPERTY(int maxWidth READ maxWidth NOTIFY maxWidthChanged)
    Q_PROPERTY(int maxHeight READ maxHeight NOTIFY maxHeightChanged)
    /** Header sizes **/
    Q_PROPERTY(int headerHeight READ headerHeight NOTIFY headerHeightChanged)
    /** Device type **/
    Q_PROPERTY(bool nType READ nType CONSTANT)
    Q_PROPERTY(int margin READ margin CONSTANT)
public:
	explicit SizeHelper(QObject *parent = 0);
	virtual ~SizeHelper();

	enum Orientation{
	    PORTRAIT = 0,
	    LANDSCAPE
	};

	static SizeHelper *instance();


signals:
    void orientationStringChanged(const QString orientationString);
    void headerHeightChanged(int headerHeight);
    void maxWidthChanged(int maxWidth);
    void maxHeightChanged(int maxHeight);
    void orientationChanged(int orientation);


public slots:
    /** Display sizes **/
	int maxWidth() const;
	int maxHeight() const;
	/** Header sizes **/
	int headerHeight() const;
	/** Orientation **/
	inline int orientation() const { return _orientation; }
	QString orientationString() const;
	/** Q10 or Q5 **/
	inline bool nType() const { return maxHeight() == maxWidth(); }

	inline int margin() const {
	    if (!nType()){
	        return 20;
	    }else{
	        if (maxWidth() == 1440){
	            return 20;
	        }else{
	            return 15;
	        }
	    }
	    return 15;
	}


private slots:
    void orientationAboutToChange(bb::cascades::UIOrientation::Type orientation);



private:
    Q_DISABLE_COPY(SizeHelper)
    static SizeHelper *_instance;
	DisplayInfo _displayInfo;
	DeviceInfo _deviceInfo;
	OrientationSupport *_orientationSupport;
	Orientation _currentOrientation;
	int _orientation;
};

#endif /* SIZEHELPER_H_ */
