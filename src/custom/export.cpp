#include "export.h"

Export *Export::_instance = 0;

#ifdef BB10
using namespace bb::cascades;
using namespace bb::cascades::pickers;
using namespace bb;
using namespace bb::system;
#endif

Export::Export(QObject *parent) :
                                                    QObject(parent),
                                                    _result(""),
                                                    _timeFormat("mm:ss:zzz")
{
#ifdef BB10
    invoker = new Invoker;
    invoker->setTarget("sys.pim.uib.email.hybridcomposer");
    invoker->setAction("bb.action.COMPOSE");
    invoker->setMimeType("message/rfc822");

    request = new InvokeRequest;
    invokeManager = new InvokeManager;
    connect(invokeManager, SIGNAL(childCardDone(bb::system::CardDoneMessage)), this, SLOT(invocationFinished()));

    connect(invoker, SIGNAL(invocationFinished()), this, SLOT(invocationFinished()));
#endif
}

Export::~Export()
{
    delete invoker;
    delete request;
    delete invokeManager;
}


Export *Export::instance()
{
    if (!_instance)
        _instance = new Export;
    return _instance;
}

void Export::destroy()
{
    if (_instance){
        delete _instance;
        _instance = 0;
    }
}

void Export::invokeEmail(const QString &fileName, const bool &attachment)
{
    request->setTarget("sys.pim.uib.email.hybridcomposer");
    request->setAction("bb.action.COMPOSE");
    request->setMimeType("message/rfc822");
    QVariantMap data;
    //data["subject"] = fileName;
    if (!attachment){
        data["body"] = result();
    }else{
        _tempFile = "shared/misc/"+fileName.trimmed();
        QFile file(_tempFile);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        file.write(result().toUtf8());
        file.close();
        qDebug() << _tempFile;
        data["attachment"] = (QVariantList() << QString(QUrl(_tempFile).toEncoded()));
    }
    QVariantMap moreData;
    moreData["data"] = data;
    bool ok;
    request->setData(PpsObject::encode(moreData, &ok));
    invokeManager->invoke(*request);
    //invoker->setData(PpsObject::encode(moreData, &ok));
    //invoker->invoke();
}

void Export::shareFile(const QString& fileName)
{
    _tempFile = QDir::currentPath()+"/shared/misc/"+fileName.trimmed();
    QFile file(_tempFile);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    file.write(result().toUtf8());
    file.close();
#ifdef QT_DEBUG
    qDebug() << _tempFile;
#endif
    invocation = Invocation::create(InvokeQuery::create().parent(this).uri("file:///" + _tempFile));
    connect(invocation, SIGNAL(armed()), this, SLOT(onArmed()));
    connect(invocation, SIGNAL(finished()), this, SLOT(invocationFinished()));
    connect(invocation, SIGNAL(finished()), invocation, SLOT(deleteLater()));
}

void Export::onArmed()
{
    invocation->trigger("bb.action.SHARE");
}


void Export::exportAllToCsv(QList<QObject *> &objects, const bool &escape, const QString &separator)
{
    _result.clear();
    emit resultChanged();
    foreach(QObject *object, objects){
        StopWatchItem *item = qobject_cast<StopWatchItem*>(object);
        exportItemToCsv(item, escape, separator);
        _result += "----------" + separator + "----------" + LF;
    }
    _result.chop(1);
    emit resultChanged();
}

void Export::exportAllToCsv(QList<StopWatchItem *> &items, const bool &escape, const QString &separator)
{
    _result.clear();
    emit resultChanged();
    foreach(StopWatchItem *item, items){
        exportItemToCsv(item, escape, separator);
        _result += "----------" + separator + "----------" + LF;
    }
    _result.chop(1);
    emit resultChanged();
}

#ifdef BB10

void Export::invocationFinished()
{
#ifdef QT_DEBUG
    qDebug() << "FINISHED" << Q_FUNC_INFO;
#endif
    QFile::remove(_tempFile);
}

void Export::exportAllToCsv(bb::cascades::GroupDataModel *model, const bool &escape, const QString &separator)
{
    _result.clear();
    emit resultChanged();
    QList<QObject*>objects = model->toListOfObjects();
    foreach(QObject *object, objects){
        StopWatchItem *item = qobject_cast<StopWatchItem*>(object);
        exportItemToCsv(item, escape, separator);
        _result += "----------" + separator + "----------" + LF;
    }
    _result.chop(1);
    emit resultChanged();
}

void Export::exportAllToTxt(bb::cascades::GroupDataModel* model)
{
    _result.clear();
    emit resultChanged();
    QList<QObject*> items = model->toListOfObjects();
    foreach(QObject *object, items){
        StopWatchItem *item = qobject_cast<StopWatchItem*>(object);
        exportItemToTxt(item);
        _result += "\n-----------------------------\n";
    }
    emit resultChanged();
}


void Export::saveToFile(const QString& fileName)
{
    FilePicker *filePicker = new FilePicker(this);
    filePicker->setType(FileType::Other);
    filePicker->setTitle(tr("Select location"));
    filePicker->setMode(FilePickerMode::Saver);
    filePicker->setDefaultSaveFileNames(QStringList() << fileName);
    filePicker->open();

    // Connect the fileSelected() signal with the slot.
    connect(filePicker,
            SIGNAL(fileSelected(const QStringList&)),
            this,
            SLOT(fileSelected(const QStringList&)));

    connect(filePicker, SIGNAL(pickerClosed()), filePicker, SLOT(deleteLater()));
    // Connect the canceled() signal with the slot.
    connect(filePicker,
            SIGNAL(canceled()),
            filePicker,
            SLOT(deleteLater()));
}


void Export::fileSelected(const QStringList& fileList)
{
    QString path = fileList.first();

    QFile file(path);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    file.write(result().toUtf8());
    file.close();
    emit fileSaved(path);
}

#endif


void Export::exportItemToCsv(StopWatchItem *item, const bool &escape, const QString &separator)
{
    _result += escapeString(tr("Name"), escape) + separator + escapeString(item->name(), escape) + LF;
    _result += escapeString(tr("Total time"), escape) + separator + escapeString(QDateTime::fromString(item->edited() ? item->eTitle() : item->title(), "mm:ss:zzz").toString(_timeFormat), escape) + LF;
    _result += escapeString(tr("Best lap"), escape) + separator + escapeString(toTime(item->bestLap()), escape) + LF;
    _result += escapeString(tr("Worst lap"), escape) + separator + escapeString(toTime(item->worstLap()), escape) + LF;
    _result += escapeString(tr("Average lap"), escape) + separator + toTime(item->averageLap()) + LF;
    _result += escapeString(tr("Latest lap"), escape) + separator + escapeString(toTime(item->latestLap().value("timestamp").toInt()), escape) + LF;
    if (item->totalLaps()){
        _result += escapeString(tr("Total laps"), escape) + separator + escapeString(QString::number(item->totalLaps()), escape) + LF;
        for (int i = 0; i < item->totalLaps(); ++i){
            int count = i+1;
            _result += escapeString(QString::number(count), escape) + separator + escapeString(toTime(item->laps().at(i).toMap().value("timestamp").toInt()), escape) + LF;
        }
    }
#ifdef QT_DEBUG
    qDebug() << _result;
#endif
    emit resultChanged();
}


void Export::exportItemToTxt(StopWatchItem* item)
{
    _result += item->name();
    _result += "\n\n";
    _result += tr("Total time");
    _result += ": ";
    _result += QDateTime::fromString(item->edited() ? item->eTitle() : item->title(), "mm:ss:zzz").toString(_timeFormat);
    _result += "\n\n";
    _result += tr("Best lap");
    _result += ": ";
    _result += toTime(item->bestLap());
    _result += "\n";
    _result += tr("Worst lap");
    _result += ": ";
    _result += toTime(item->worstLap());
    _result += "\n";
    _result += tr("Average lap");
    _result += ": ";
    _result += toTime(item->averageLap());
    _result += "\n";
    _result += tr("Latest lap");
    _result += ": ";
    _result += toTime(item->latestLap().value("timestamp").toInt());
    if (item->totalLaps()){
        _result += "\n\n";
        _result += tr("Laps");
        _result += "\n";
        for (int i = 0; i < item->totalLaps(); ++i){
            int count = i+1;
            _result += QString::number(count) + ". " + toTime(item->laps().at(i).toMap().value("timestamp").toInt());
            _result += "\n";
        }
        _result.chop(1);
    }
    emit resultChanged();
}


QString Export::toTime(const int &timestamp)
{
    return QDateTime::fromMSecsSinceEpoch(timestamp).toString(_timeFormat);
}


QString Export::escapeString(const QString &value, const bool &escape)
{
    if (escape)
        return QString("\"%1\"").arg(value);
    return value;
}

