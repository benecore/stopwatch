/*
 * activeframe.cpp
 *
 *  Created on: 8.8.2014
 *      Author: benecore
 */

#include <src/activeframe.h>

using namespace bb::cascades;

ActiveFrame::ActiveFrame(QObject *parent) :
                SceneCover(parent)
{
    qml = QmlDocument::create("asset:///Cover.qml").parent(parent);

    mainContainer = qml->createRootObject<Container>();
    setContent(mainContainer);
}

ActiveFrame::~ActiveFrame()
{
}

void ActiveFrame::updateLabel(const QString& text)
{
}

void ActiveFrame::updateData(bb::cascades::GroupDataModel* model)
{
    count = model->size();
}
