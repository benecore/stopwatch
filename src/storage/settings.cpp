/*
 * settings.cpp
 *
 *  Created on: 27.7.2014
 *      Author: benecore
 */

#include <src/storage/settings.h>

Settings *Settings::_instance = 0;

Settings::Settings(QObject *parent) :
    QObject(parent),
    s(0)
{
#ifdef BB10
    s = new QSettings("data/settings.ini", QSettings::IniFormat);
#endif

    _color = s->value("color", "0098f0");
    _showCustomizeDialog = s->value("showCustomizeDialog", false).toBool();
    _frameBattery = s->value("frameBattery", false).toBool();
    // Export settings
    _fileFormat = s->value("fileFormat", 0).toInt();
    _fileNameFormat = s->value("fileNameFormat", 0).toInt();
    _csvDelimeter = s->value("csvDelimeter", 0).toInt();
    _timeFormat = s->value("timeFormat", 0).toInt();
    _timeFormatString = s->value("timeFormatString", "").toString();
}

Settings::~Settings()
{
    delete s;
}

Settings* Settings::instance()
{
    if (!_instance)
        _instance = new Settings;
    return _instance;
}

void Settings::destroy()
{
    if (_instance){
        delete _instance;
        _instance = 0;
    }
}
