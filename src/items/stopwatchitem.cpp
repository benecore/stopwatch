#include "stopwatchitem.h"

StopWatchItem::StopWatchItem(QObject *parent) :
QObject(parent),
_id(0),
_name(""),
_running(false),
_latestLap(getLap(0)),
_color("#0098f0"),
_timerSec(0),
_timerMicro(0),
_sTime(0),
_mTime(0),
_modulo(0),
_edited(false)

{
    _timeElapsed = 0;
}

StopWatchItem::~StopWatchItem()
{
    if (_timerSec) delete _timerSec;
    if (_timerMicro) delete _timerMicro;
}

void StopWatchItem::start()
{
    if (_running)
        return;
    _running = true;
    emit dataChanged();

    _startTime = QDateTime::currentDateTimeUtc();
    _startTimeMsec = _startTime.toMSecsSinceEpoch();

    _timerSec = new QTimer(this);
    _timerMicro = new QTimer(this);

    connect(_timerSec,   SIGNAL(timeout()), this, SLOT(timerSecSlot()));
    connect(_timerMicro, SIGNAL(timeout()), this, SLOT(timerMicroSlot()));

    _timerMicro->start(75); // 95
    _timerSec->start(200); // 200
}

void StopWatchItem::stop()
{
    if (!_running)
        return;
    _running = false;
    emit dataChanged();

    _timerMicro->stop();
    _timerSec->stop();

    QDateTime now = QDateTime::currentDateTimeUtc();
    _timeElapsed += now.toMSecsSinceEpoch() - _startTimeMsec;
}

void StopWatchItem::lap()
{
    QDateTime lap;
    lap.setTimeSpec(Qt::UTC);
    QDateTime now = QDateTime::currentDateTimeUtc();
    QDateTime timeValue;
    timeValue.setTimeSpec(Qt::UTC);
    timeValue.setMSecsSinceEpoch(now.toMSecsSinceEpoch() - _startTimeMsec + _timeElapsed);

    if (_laps.size()){
        int timestamp = timeValue.toMSecsSinceEpoch() - _latest.toMSecsSinceEpoch();
        lap.setMSecsSinceEpoch(timestamp);
    }
    _laps.append(getLap(_latest.isValid() ? lap.toMSecsSinceEpoch() : timeValue.toMSecsSinceEpoch()));
#ifdef QT_DEBUG
    if (_latest.isValid())
        qDebug() << lap.toString("hh:mm:ss:zzz") << endl << lap.toMSecsSinceEpoch();
    else
        qDebug() << timeValue.toString("hh:mm:ss:zzz") << endl << timeValue.toMSecsSinceEpoch();
#endif
    _latest = timeValue;
    emit lapsChanged(_laps);
}


void StopWatchItem::reset()
{
    if (!_running){
        _sTime = 0;
        _mTime = 0;
        _modulo = 0;
        _edited = false;
        _timeElapsed = 0;
        _latestLap = getLap(0);
        _latest = QDateTime();
        _laps.clear();
        emit dataChanged();
        emit lapsChanged(_laps);
    }
}

void StopWatchItem::timerSecSlot()
{
    QDateTime now = QDateTime::currentDateTimeUtc();
    QDateTime timeValue;
    timeValue.setTimeSpec(Qt::UTC);
    timeValue.setMSecsSinceEpoch(now.toMSecsSinceEpoch() - _startTimeMsec + _timeElapsed);


    _sTime = timeValue.toMSecsSinceEpoch();
#ifdef QT_DEBUG
    qDebug() << timeValue.toString("mm:ss");
#endif
    emit dataChanged();
}

bool StopWatchItem::removeLap(const QVariant& lap)
{
    QVariantMap tempLap = _laps.at(_laps.indexOf(lap)).toMap();

    bool edited = tempLap.value("edited").toBool();
    if (edited)
        _modulo -= tempLap.value("editedTime").toInt();
    bool check = _laps.removeOne(lap);
    if (check){
        if (_laps.isEmpty()){
            _latest = QDateTime();
            _edited = false;
        }
        if (!_running)
            emit dataChanged();
        emit lapsChanged(_laps);
    }
    return check;
}

void StopWatchItem::timerMicroSlot()
{
    QDateTime now = QDateTime::currentDateTimeUtc();
    QDateTime timeValue;
    timeValue.setTimeSpec(Qt::UTC);
    timeValue.setMSecsSinceEpoch(now.toMSecsSinceEpoch() - _startTimeMsec + _timeElapsed);

    _mTime = timeValue.toMSecsSinceEpoch();
#ifdef QT_DEBUG
    qDebug() << timeValue.toString(":zzz");
#endif
    emit dataChanged();
}

QVariant StopWatchItem::editLap(const QVariant& lap, const int& mm, const int& ss, const int& ms)
{
    int indexOf = _laps.indexOf(lap);
    if (indexOf != -1){
        QVariantMap tempLap = _laps.at(indexOf).toMap();
        QDateTime tempDateTime = QDateTime::fromMSecsSinceEpoch(tempLap.value("timestamp").toInt());
        tempDateTime.setTimeSpec(Qt::UTC);
        tempDateTime.setTime(QTime(0, mm, ss, ms));

        int oldTime = tempLap.value("timestamp").toInt();
        int newTime = tempDateTime.toMSecsSinceEpoch();

        if (oldTime != newTime){
            _edited = true;
            int _lapModulo = QDateTime::fromMSecsSinceEpoch(oldTime).toUTC().msecsTo(QDateTime::fromMSecsSinceEpoch(newTime).toUTC());
            _modulo += _lapModulo;
            if (!_running){
                emit dataChanged();
            }
#ifdef QT_DEBUG
            qDebug() << "ROZDIEL" << QDateTime::fromMSecsSinceEpoch(_modulo).toString("mm:ss:zzz") << endl << \
                    "ROZDIEL TIMESTAMP" << _modulo;
#endif
            QVariantMap newLap = getLap(newTime);
            newLap.insert("edited", true);
            newLap.insert("editedTime", _lapModulo);
            newLap.insert("originalTime", tempLap.value("lap"));
            _laps.replace(indexOf, newLap);
            emit lapsChanged(_laps);
            return newLap;
        }
        return tempLap;
    }
    return QVariant();
}
