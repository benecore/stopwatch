/*
 * activeframe.h
 *
 *  Created on: 8.8.2014
 *      Author: benecore
 */

#ifndef ACTIVEFRAME_H_
#define ACTIVEFRAME_H_

#include <bb/cascades/SceneCover>
#include <bb/cascades/Container>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/GroupDataModel>

class ActiveFrame: public bb::cascades::SceneCover
{
    Q_OBJECT
public:
    ActiveFrame(QObject *parent = 0);
    virtual ~ActiveFrame();

    Q_INVOKABLE
    void updateLabel(const QString &text);
    Q_INVOKABLE
    void updateData(bb::cascades::GroupDataModel *model);



private:
    bb::cascades::QmlDocument *qml;
    bb::cascades::Container *mainContainer;
    QString _label;
    int count;
};

#endif /* ACTIVEFRAME_H_ */
