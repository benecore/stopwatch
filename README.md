# Stopwatch #

### Multiple stopwatch for BlackBerry 10 ###

Release notes
### v1.0.1.4 (`27.08.2014`)
- Added 'Send e-mail' option
### v1.0.1.3 (`20.08.2014`)
- Performance improvements and minor changes
### v1.0.1.2 (`19.08.2014`)
- Export to CSV
- Quick action for sharing Text/CSV file
- Improved Cover for Ntype devices
- Minor changes
### v1.0.0.0 (`08.08.2014`)
- Initial version