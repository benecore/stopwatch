APP_NAME = StopWatch

CONFIG += qt warn_on cascades10

DEFINES += BB10
LIBS += -lbbdevice -lbb -lbbsystem -lbbplatform -lbbplatformbbm -lbbcascadespickers
CODECFORTR = UTF-8
CODECFORSRC = UTF-8
CODEFORTR = UTF-8
TRANSLATIONS = \
StopWatch_sk.cs

device {
    CONFIG(release, debug|release) {
        DESTDIR = o.le-v7
        TEMPLATE=lib
        QMAKE_CXXFLAGS += -fvisibility=hidden
    }
    CONFIG(debug, debug|release) {
        DESTDIR = o.le-v7-g
    }
}
 
simulator {
    CONFIG(release, debug|release) {
        DESTDIR = o
    }
    CONFIG(debug, debug|release) { 
        DESTDIR = o-g
    } 
}
 
OBJECTS_DIR = $${DESTDIR}/.obj
MOC_DIR = $${DESTDIR}/.moc
RCC_DIR = $${DESTDIR}/.rcc
UI_DIR = $${DESTDIR}/.ui


include(config.pri)
