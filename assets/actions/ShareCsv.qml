import bb.cascades 1.2

ActionItem{
    id: root
    signal clicked
    
    property bool freeVersion: app.freeVersion
    
    enabled: !freeVersion
    title: qsTr("Share CSV").concat(freeVersion ? " (PRO)" : "") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/ic_share_csv_".concat(Qt.app.currentItem.colorAccent.replace("#", ""))+".png"
    onTriggered: {
        root.clicked()
    }   
}