import bb.cascades 1.2

ActionItem {
    id: root
    
    signal clicked
    
    title: qsTr("Customize") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/ic_beautify_inactive.png"
    
    onTriggered: {
        root.clicked()
    }   
}