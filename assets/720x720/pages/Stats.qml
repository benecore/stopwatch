import bb.cascades 1.2

import "../components"

Container {
    id: rootContainer
    layout: StackLayout {
        orientation: LayoutOrientation.TopToBottom
    }
    preferredWidth: Qt.sizeHelper.maxWidth
    verticalAlignment: VerticalAlignment.Fill
    Container {
        layout: DockLayout {}
        horizontalAlignment: HorizontalAlignment.Center
        layoutProperties: StackLayoutProperties {
            spaceQuota: 1
        }
        MyLabel {
            verticalAlignment: VerticalAlignment.Center
            text: Qt.app.currentItem.edited ? Qt.app.currentItem.eTitle : Qt.app.currentItem.title
            color: Color.create(Qt.app.currentItem.colorAccent)
            size: 36
            bottomMargin: 0
        } // end of TitleLabel
        MyLabel {
            topMargin: 0
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Bottom
            visible: Qt.app.currentItem.edited
            text: Qt.app.currentItem.title
            color: Color.create(Qt.app.currentItem.colorAccent)
            size: 10
        } // end of TitleLabel
        bottomMargin: 0
        topMargin: 0
    }
    
    LapStatsHeader {
        
    }
    
    LapStatisticsPortrait {
    }
    
    Divider {
        topMargin: 0
    }
    
    // --------------------------------------------------------- //
} // end of root container