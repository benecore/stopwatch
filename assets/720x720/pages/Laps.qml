import bb.cascades 1.2

import "../components"
import "../items"

Container {
    id: root
    
    function appendLap(lap){
        if (lap.length && !editing && !removing){
            model.append(Qt.app.currentItem.latestLap)
            listView.scrollToItem([model.size()-1], ScrollAnimation.Smooth)
        }
        else if (!lap.length){
            clearLaps()
        }
    }
    
    
    function loadLaps(laps){
        model.clear()
        model.append(laps)
    }
    
    
    function clearLaps(){
        model.clear()
    }
    
    onCreationCompleted: {
        Qt.app.currentItem.lapsChanged.connect(appendLap)
        loadLaps(Qt.app.currentItem.laps)
    }
    
    
    attachedObjects: [
        ComponentDefinition {
            id: editDialog
            EditDialog {
            
            }
        }
    ]
    
    property bool removing: false
    property bool editing: false
    layout: DockLayout{}
    preferredWidth: Qt.sizeHelper.maxWidth
    preferredHeight: preferredWidth
    ListView {
        id: listView
        topPadding: 10
        scrollRole: ScrollRole.Main
        visible: Qt.app.currentItem.totalLaps
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        layout: StackListLayout {
            orientation: LayoutOrientation.TopToBottom
        
        }
        dataModel: ArrayDataModel {
            id: model
        }
        
        function removeLap(indexPath){
            removing = true
            var item = dataModel.data(indexPath)
            if (item){
                var check = Qt.app.currentItem.removeLap(item)
                console.log("CHECK "+check)
                if (check){
                    dataModel.removeAt(indexPath)
                }
                removing = false
            }
        }
        
        function edit(indexPath){
            clearSelection()
            select(indexPath)
            var item = dataModel.data(indexPath)
            if (item){
                editing = true
                var dialog = editDialog.createObject()
                dialog.setLap(item)
                dialog.saved.connect(updateLap)
                dialog.open()
            }
        }
        
        function updateLap(newLap){
            var indexPath = selected();
            if (newLap){
                model.replace(indexPath, newLap)
                model.itemUpdated(indexPath)
                editing = false;
            }
        }
        
        listItemComponents: [
            ListItemComponent {
                type: ""
                ListItemLaps{}
            }
        ] 
    } // end of listview
    
    
    Container {
        visible: !Qt.app.currentItem.totalLaps
        horizontalAlignment: HorizontalAlignment.Center
        MyLabel {
            opacity: 0.5
            text: qsTr("No laps") + Retranslate.onLocaleOrLanguageChanged
            size: 20
            color: Color.Gray
        }
    } // end of No Laps

}