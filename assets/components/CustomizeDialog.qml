import bb.cascades 1.2


CustomDialog {
    id: root
    
    property variant item
    
    property int imageHeight: sizeHelper.nType ? 120 : 140
    
    title: item ? item.name : ""
    lineColor: item ? Color.create(item.colorAccent) : Color.create("#f8f8f8")
    rightBtnVisible: false
    
    onDone: {
        close()
    }
    
    onCancel: {
        if (item && nameField.text.length) item.name = nameField.text.trim()
        close()
    }
    
    Container {
        id: rootContainer
        leftPadding: 20
        rightPadding: 20
        topPadding: 20
        bottomPadding: 20
        
        TextField {
            id: nameField
            hintText: qsTr("name") + Retranslate.onLocaleOrLanguageChanged
            inputMode: TextFieldInputMode.Text
            input.submitKey: SubmitKey.EnterKey
            text: item ? item.name : ""
            input{
                onSubmitted: {
                    if (text.length && item)
                        item.name = text.trim()
                
                }
            }
        } // end of textField
        
        ListView {
            visible: false
            layout: StackListLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Center
            preferredHeight: imageHeight
            onCreationCompleted: {
                var items = ["0098f0", "96b800", "cc3f10", "a30d7e"]
                model.append(items)
            }
            dataModel: ArrayDataModel {
                id: model
            }
            function getItem(){
                return root.item;
            }
            listItemComponents: [
                ListItemComponent {
                    type: ""
                    ImageView {
                        id: rootItem
                        scaleX: rootItem.ListItem.view.getItem().colorAccent === "#".concat(ListItemData) ? 0.8 : 1
                        scaleY: scaleX
                        preferredWidth: imageHeight
                        scalingMethod: ScalingMethod.AspectFit
                        imageSource: "asset:///images/colors/".concat(ListItemData).concat(".png")
                    }
                }
            ]
            onTriggered: {
                var item = dataModel.data(indexPath)
                if (item){
                    root.item.colorAccent = "#".concat(item)
                }
            }
            scrollIndicatorMode: ScrollIndicatorMode.None
        } // end of ListView
        
        
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Center
            ImageButton {
                preferredWidth: imageHeight
                preferredHeight: preferredWidth
                scaleX: item ? (item.colorAccent === "#0098f0") ? 0.8 : 1 : 1
                scaleY: scaleX
                defaultImageSource: "asset:///images/colors/".concat("0098f0").concat(".png")
                pressedImageSource: defaultImageSource
                onClicked: {
                    if (item)
                        item.colorAccent = "#0098f0"
                }
            }
            ImageButton {
                preferredWidth: imageHeight
                preferredHeight: preferredWidth
                scaleX: item ? (item.colorAccent === "#96b800") ? 0.8 : 1 : 1
                scaleY: scaleX
                defaultImageSource: "asset:///images/colors/".concat("96b800").concat(".png")
                pressedImageSource: defaultImageSource
                onClicked: {
                    if (item)
                        item.colorAccent = "#96b800"
                }
            }
            ImageButton {
                preferredWidth: imageHeight
                preferredHeight: preferredWidth
                scaleX: item ? (item.colorAccent === "#cc3f10") ? 0.8 : 1 : 1
                scaleY: scaleX
                defaultImageSource: "asset:///images/colors/".concat("cc3f10").concat(".png")
                pressedImageSource: defaultImageSource
                onClicked: {
                    if (item)
                        item.colorAccent = "#cc3f10"
                }
            }
            ImageButton {
                preferredWidth: imageHeight
                preferredHeight: preferredWidth
                scaleX: item ? (item.colorAccent === "#a30d7e") ? 0.8 : 1 : 1
                scaleY: scaleX
                defaultImageSource: "asset:///images/colors/".concat("a30d7e").concat(".png")
                pressedImageSource: defaultImageSource
                onClicked: {
                    if (item)
                        item.colorAccent = "#a30d7e"
                }
            }
        }
    } // end of rootContainer
    
}