import bb.cascades 1.2

NavigationPane {
    id: root
    
    
    onPushTransitionEnded: {
        page.active = true
    }
    
    onPopTransitionEnded: {
        page.active = false
        if (page.destroy) app.deleteLater(page)
    }
    
}