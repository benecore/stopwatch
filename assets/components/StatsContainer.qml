import bb.cascades 1.2

Container {
    id: root
    
    property alias key: keyLabel.text
    property alias keySize: keyLabel.size
    property alias keyBold: keyLabel.bold
    property alias value: valueLabel.text
    property alias valueSize: valueLabel.size
    property alias valueBold: valueLabel.bold
    
    MyLabel {
        horizontalAlignment: HorizontalAlignment.Center
        id: keyLabel
    }
    
    
    MyLabel {
        horizontalAlignment: HorizontalAlignment.Center
        id: valueLabel
    }
    
}