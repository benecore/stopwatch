import bb.cascades 1.2

TitleBar {
    id: root
    
    
    property bool dotsVisible: false
    property int dotIndex: 0
    
    
    kind: TitleBarKind.FreeForm
    kindProperties: FreeFormTitleBarKindProperties {
        Container {
            layout: DockLayout{}
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            background: app.whiteTheme ? Color.create("#f5f5f5") : Color.create("#262626")
            
            Container {
                id: lineContainer
                visible: !app.newOs
                layout: DockLayout{}
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Bottom
                preferredHeight: 3
                background: Color.create("#".concat(settings.color))
            }
            
            Container {
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                
                Label {
                    text: root.title
                    textStyle{
                        color: app.whiteTheme ? Color.Black : Color.White
                        fontSize: FontSize.PointValue
                        fontSizeValue: 13
                        fontWeight: FontWeight.W200
                    }
                    bottomMargin: 0
                } // end of label
                
                Container {
                    topMargin: 5
                    visible: dotsVisible
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    bottomPadding: 5
                    ImageView {
                        rightMargin: 0
                        preferredHeight: 30
                        scalingMethod: ScalingMethod.AspectFit
                        imageSource: (dotIndex == 0) ? "asset:///images/colors/".concat(app.currentItem ? app.currentItem.colorAccent.replace("#", "") : "0098f0").concat(".png") : "asset:///images/colors/white.png"
                    }
                    ImageView {
                        leftMargin: 0
                        preferredHeight: 30
                        scalingMethod: ScalingMethod.AspectFit
                        //imageSource: "asset:///images/".concat(Qt.settings.color).concat(".png")
                        imageSource: (dotIndex == 1) ? "asset:///images/colors/".concat(app.currentItem ? app.currentItem.colorAccent.replace("#", "") : "0098f0").concat(".png") : "asset:///images/colors/white.png"
                    }
                    
                    
                } // images layout 
            }
        } // end of container
    }
    scrollBehavior: TitleBarScrollBehavior.Sticky // end of free forms

}