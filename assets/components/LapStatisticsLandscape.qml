import bb.cascades 1.2


Container {
    id: statsContainer
    layout: StackLayout {
        
    }
    preferredWidth: sizeHelper.maxWidth/2

    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Top
        StatsContainer {
            topPadding: 5
            bottomPadding: 5
            leftPadding: 5
            rightPadding: 5
            minWidth: sizeHelper.maxWidth/4
            
            key: qsTr("Best lap") + Retranslate.onLocaleOrLanguageChanged
            keySize: 6
            keyBold: FontWeight.Bold
            
            value: app.currentItem ? Qt.formatTime(new Date(app.currentItem.bestLap), "mm:ss:zzz") : ""
            valueSize: 7
        } // end of BestLap
        
        Container {
            background: app.whiteTheme ? Color.Black : Color.LightGray
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Left
            maxHeight: Infinity                        
            minWidth: 1
            maxWidth: 1
        }
        
        StatsContainer {
            topPadding: 5
            bottomPadding: 5
            leftPadding: 5
            rightPadding: 5
            minWidth: sizeHelper.maxWidth/4
            
            key: qsTr("Worst lap") + Retranslate.onLocaleOrLanguageChanged
            keySize: 6
            keyBold: FontWeight.Bold
            
            value: app.currentItem ? Qt.formatTime(new Date(app.currentItem.worstLap), "mm:ss:zzz") : ""
            valueSize: 7
        
        } // end of WorstLap
    }
    
    Divider {
        topMargin: 0
        bottomMargin: 0
    }
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        StatsContainer {
            topPadding: 5
            bottomPadding: 5
            leftPadding: 5
            rightPadding: 5
            minWidth: sizeHelper.maxWidth/4
            
            key: qsTr("Average lap") + Retranslate.onLocaleOrLanguageChanged
            keySize: 6
            keyBold: FontWeight.Bold
            
            value: app.currentItem ? Qt.formatTime(new Date(app.currentItem.averageLap), "mm:ss:zzz") : ""
            valueSize: 7
        
        } // end of AverageLap
        
        Container {
            background: app.whiteTheme ? Color.Black : Color.LightGray
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Left
            maxHeight: Infinity                        
            minWidth: 1
            maxWidth: 1
        }
        
        StatsContainer {
            topPadding: 5
            bottomPadding: 5
            leftPadding: 5
            rightPadding: 5
            minWidth: sizeHelper.maxWidth/4
            
            key: qsTr("Latest lap") + Retranslate.onLocaleOrLanguageChanged
            keySize: 6
            keyBold: FontWeight.Bold
            
            value: app.currentItem ? app.currentItem.latestLap.lap : ""
            valueSize: 7
        
        } // end of latestLap
    }
    
    
}