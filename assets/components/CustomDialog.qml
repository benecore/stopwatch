import bb.cascades 1.2
import bb.system 1.2

Dialog {
    id: root
    
    property alias title: label.text
    default property alias contentItems: contentContainer.controls
    property alias height: contentContainer.preferredHeight
    property alias leftBtnText: leftBtn.text
    property alias rightBtnText: rightBtn.text
    property alias leftBtnVisible: leftBtn.visible
    property alias rightBtnVisible: rightBtn.visible
    property alias lineColor: lineContainer.background
    
    signal cancel()
    signal done()
    
    function numericOnly (textin) {
        var m_strOut = new String (textin);
        m_strOut = m_strOut.replace(/[^\d.]/g,'');
        return m_strOut;
    }
    
    Container {
        preferredHeight: Qt.sizeHelper.maxHeight
        preferredWidth: Qt.sizeHelper.maxWidth
        background: Color.create(0.0, 0.0, 0.0, 0.5)
        layout: DockLayout{}
        
        
        Container {
            layout: DockLayout{}
            topPadding: 60
            preferredWidth: Qt.sizeHelper.nType ? 650 : 700
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Top
            
            
            Container {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                background: Qt.app.whiteTheme ? Color.create("#f8f8f8") : Color.create("#262626")
                
                Container {
                    layout: DockLayout{}
                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Fill
                    background: Qt.app.whiteTheme ? Color.create("#f5f5f5") : Color.create("#262626")
                    preferredHeight: Qt.sizeHelper.nType ? 85 : 105
                    
                    Container {
                        id: lineContainer
                        layout: DockLayout{}
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Bottom
                        preferredHeight: 5
                    }
                    
                    Container {
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        
                        Label {
                            id: label
                            textStyle{
                                color: Qt.app.whiteTheme ? Color.Black : Color.White 
                                fontSize: FontSize.PointValue
                                fontSizeValue: 13
                                fontWeight: FontWeight.W200
                            }
                            bottomMargin: 0
                        } // end of label
                    }
                } // end Header
                Container {
                    id: contentContainer
                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Fill
                    leftPadding: 15
                    rightPadding: 15
                    
                } // end of content container
                Container {
                    id: btnContainer
                    topMargin: 0
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Bottom
                    leftPadding: 10
                    rightPadding: 10
                    bottomPadding: 10
                    Button {
                        id: leftBtn
                        text: qsTr("Close") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            root.cancel()
                        }
                    }
                    Button {
                        id: rightBtn
                        text: qsTr("Save") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            root.done()
                        }
                    }
                } // end of buttons
            } // end of stacklayout container
        }
    } // end of root Container
}